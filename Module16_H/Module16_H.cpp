﻿#include <iostream>
#define N 20


using namespace std;


int main()
{
    int arr[N][N];
    int sum;
    int number;
    cin >> number;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            arr[i][j] = i + j;
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }


    for (int i = 0; i < N; i++)
    {
        sum = 0;
        if (number % N != i)
            continue;
        for (int j = 0; j < N; j++)
            sum += arr[i][j];
        cout << i << ": " << sum << endl;
    }

}